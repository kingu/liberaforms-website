---
title: Inicio
published: true
slug: inicio
---

# Formularios éticos con LiberaForms
LiberaForms es una herramienta de software libre pensada y desarrollada como infraestructura comunitaria, libre y ética que permite crear y gestionar formularios que respetan los derechos digitales de las personas que hacen uso de ellos.

Con LiberaForms puedes consultar, editar y descargar las respuestas recibidas; incluir una casilla para pedir consentimiento sobre la Ley de Protección de Datos; colaborar con otras usuarias compartiendo permisos; y muchas cosas más! 

Conoce mejor [el proyecto!](/proyecto) 

LiberaForms es cultura libre bajo licencia AGPLv3. ¡Úsalo, compártelo y mejóralo!

* Echa [un vistazo](/proyecto/media)
* Si quieres probar LiberaForms, [rellena este formulario](https://usem.liberaforms.org/peticion)
* Si quieres instalar tu propio LiberaForms, [consulta el código](https://gitlab.com/liberaforms/liberaforms)
