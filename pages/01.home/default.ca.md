---
title: Inici
published: true
slug: inici
---

# Formularis ètics amb LiberaForms
LiberaForms és una eina de programari lliure pensada i desenvolupada com a infraestructura comunitària, lliure i ètica que permet crear i gestionar formularis que respecten els drets digitals de les persones que en fan ús.

Amb LiberaForms pots consultar, editar i descarregar les respostes rebudes; incloure una casella per a demanar consentiment sobre la Llei de Protecció de Dades; col·laborar amb altres usuàries compartint permissos; i moltes coses més!

Coneix millor [el projecte!](/projecte) 

LiberaForms és cultura lliure sota llicència AGPLv3: usa'l, comparteix-lo i millora'l!

* Fes [una ullada](/projecte/media)
* Si vols provar LiberaForms, [omple aquest formulari](https://usem.liberaforms.org/peticio)
* Si vols instal·lar una instància de LiberaForms, [consulta el codi](https://gitlab.com/liberaforms)