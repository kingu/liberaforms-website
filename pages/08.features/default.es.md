---
title: Funcionalidades
published: false
slug: funcionalidades
---

### Funcionalidades

Las **funcionalidades de usuaria** de LiberaForms son: 

* Crear formularios en línea
* Definir la pantalla de 'Gracias!', que aparece una vez se rellena un formulario 
* Definir la pantalla de 'Este formulario ha caducado'
* Definir las condiciones de expiración (fecha, campos numéricos)
* Consultar las entradas por tabla, gráfico, CSV
* Recibir notificaciones por correo electrónico
* Incluir un texto propio de Ley de Protección de Datos (LOPD) y otros textos de contenimiento
* Incluir una casilla para enviar un acuse de recibo 
* Incluir un enlace de incrustación

Las funcionalidades de usuaria no incluyen el acceso a la administración del software ni la gestión de las usuarias. Opciones para acceder a las funcionalidades de administración y gestión: si tenéis los conocimientos para hacerlo, podéis instalar directamente el software en un servidor y instalar LiberaForms bajo vuestro dominio. Aquí tenéis el [código](https://gitlab.com/la-loka/liberaforms). Si no tenéis los conocimientos para implementar y mantener una instancia de LiberaForms, os podemos ofrecer una [cuota con dominio propio](https://liberaforms.org/es/servicios). 

Las **funcionalidades de administración y gestión** de LiberaForms son: 

* Crear / Administrar usuarias
* Definir textos por omisión de LOPD, Temrinos y condiciones, etc...
* Trabajo en equipo  (donde usuarias pueden consultar/usar formularios creados por otras usuarias de la instancia)
* Configuración SMTP
* Personalizar la web poniendo tu marca gráfica 
  * Texto de la página de inicio
  * Nombre del sitio web
  * Icono (favicon)
  * Color de menú
  
