---
title: Funtzionalitateak
published: false
slug: funtzionalitateak
---

### Funtzionalitateak

LiberaForms-eko **erabiltzailearen funtzionalitateak** hauek dira:

* Lineako galdetegiak sortu
* 'Esker aunitz!' ikuspegia zehaztu, galdetegi bat bete ondoren agertzen dena
* 'Galdetegi hau iraungi da' ikuspegia zehaztu
* Iraungitze-baldintzak zehaztu (data, eremu numerikoak)
* Erantzunak taula, grafiko edo CSV bidez kontsultatu
* Jakinarazpenak posta elektronikoz jaso
* Datuak Pertsonalen Babeserako Lege Organikoaren (DPBLO) testu propioa zehaztu
* Lauki bat sartu jaso izanaren agiria bidaltzeko
* Galdetegia beste web-orri batean txertatzeko esteka gehitu

Erabiltzailearen funtzionalitateetan ez dira sartzen ez softwarearen administrazioa, ez erabiltzaileen kudeaketa. Administrazio- eta kudeaketa-funtzionalitateetara sartzeko aukerak: horretarako ezagutzak badituzue, softwarea zuzenean instalatu dezakezue zerbitzari batean eta LiberaForms zuen domeinuan instaalatu. Hemen duzue [kodea](https://gitlab.com/la-loka/liberaforms). Ez baduzue LiberaForms instantzia bat ekin eta mantentzeko jakintza, [domeinu propiodun kuota](https://liberaforms.org/eu/zerbitzuak) eskaintzen dizuegu. 

LiberaForms-eko **administrazio-funtzionalitateak** hauek dira:

* Erabiltzaileak sortu / administratu
* Testu propioak zehaztu: DPBLO, erabilera-baldintzak...
* Taldean lana egin (erabiltzaileek instantzia bereko beste erabiltzaileek sortutako galdetegiak ikuskatu/erabili ditzakete)
* SMTP konfigurazioa
* Webgunea pertsonalizatu zure marka grafikoa jarriz
  * Hasiera-orriko testua
  * Webgunearen izena
  * Ikonoa (favicon-a)
  * Menuaren kolorea
  
