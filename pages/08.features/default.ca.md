---
title: Funcionalitats
published: false
slug: funcionalitats
---

### Funcionalitats  

Les **funcionalitats d'usuària** de LiberaForms són: 

* Crear formularis en línia
* Definir la pantalla de 'Gràcies!', que apareix un cop s'omple un formulari 
* Definir la pantalla de 'Aquest formulari ha caducat'
* Definir les condicions d'expiració (data, camps numèrics)
* Consultar les entrades per taula, gràfic, CSV
* Rebre notificacions per correu electrònic
* Incloure un text propi de Llei de Protecció de Dades (LOPD) 
* Incloure una casella per enviar un justificant de recepció 
* Incloure un enllaç d'inscrustació 

Les funcionalitats d'usuària no inclouen l'accés a l'administració del programari ni la gestió d'usuàries. Opcions per accedir a funcionalitats d'administració i gestió: si teniu els coneixements per fer-ho, podeu instal·lar directament el programari a un servidor i instal·lar LiberaForms sota el vostre propi domini. Aquí teniu el [codi](https://gitlab.com/la-loka/liberaforms). Si no teniu els coneixements per implementar i mantenir una instància de LiberaForms, però voleu gaudir de les funcionalitats d'administració i gestió, us podem oferir una [quota amb domini propi](https://liberaforms.org/ca/serveis). 

Les **funcionalitats d'administració i gestió** de LiberaForms són: 

* Crear / Administrar usuàries
* Definir textos per omissió de LOPD, Termes i condicions, etc. 
* Treball en equip (on usuàries poden consultar/usar formularis creats per altres usuàries de la instància)
* Configuració SMTP
* Personalitzar la web posant-hi la teva marca gràfica
  * Text de la pàgina d'inici
  * Nom del lloc web
  * Icona (favicon)
  * Color del menú
  
