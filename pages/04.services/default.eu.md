---
title: Zerbitzuak
published: true
slug: zerbitzuak
---

# Zerbitzuak
Eskaintzen ditugun zerbitzuen modalitateak eta baldintzak. BEZik gabeko prezioak. Proposatutako zerbitzuak zuen proiektuarekin bat ez badatoz, jarri gurekin harremanetan **info [@] liberaforms.org** helbidean, eta azaldu zer behar duzuen neurrira egindako kuota bat egiteko.

![zerbitzuen|komunen deskribapen grafikoa](liberafoms-modalitats-eu.png "zerbitzuen|komunen deskribapen grafikoa")

## Doakoa
* Domeinu propiorik gabe: "liberaforms.org" domeinuko URL batekin bidaliko dira galdetegiak.  
Adibidez: [https://erabili.liberaforms.org/eskaera](https://erabili.liberaforms.org/eskaera)  
Erabili galdetegi hau doako modalitate honetako erabiltzaile bat sortzeko.
* Urtean gehienez ere 200 erantzun. Nahi adina galdetegi bidali ditzakezu, baina urtean gehienez 200 erantzun jaso ahal izango dituzu.

Aukera honek erabiltzailearen funtzionalitateak ditu:

* Lineako galdetegiak sortu eta argitaratu
* Erantzunak taula, grafiko edo CSV bidez kontsultatu
* 'Esker aunitz!' ikuspegia zehaztu, galdetegi bat bete ondoren agertzen dena
* Jakinarazpenak posta elektronikoz jaso
* Datuak Pertsonalen Babeserako Lege Organikoaren (DPBLO) testu propioa zehaztu
* Lauki bat sartu jaso izanaren agiria bidaltzeko
* Iraungitze-baldintzak zehaztu (data, eremu numerikoak)
* 'Galdetegi hau iraungi da' ikuspegia zehaztu
* Galdetegia beste web-orri batean txertatzeko esteka gehitu

## Domeinu propiorik gabeko kuota
* Doako modalitatean bezala, galdetegiak URL batekin bidaliko dira, "liberaforms.org" domeinuarekin, eta erabiltzailearen funtzionalitateak ditu.
* Hilean gehienez ere 800 erantzun. Nahi adina galdetegi bidali ditzakezu, baina hilean gehienez 800 erantzun jaso ahal izango dituzu.
* Tarifak: 10 € hilabetean, 25 € hiruhilekoan, 90 € urtean

## Domeinu propiodun kuota
* Domeinu propioa duen kuota batek beste domeinu baten pean galdetegiak sortzeko aukera ematen du.  
Adibidez: https://galdetegiak.ekipamendulibreak.eus/  
Aukera honi esker, behean zehazten ditugun administrazio- eta kudeaketa-funtzionalitateak erabili daitezke.
* Hilean gehienez ere 3.000 erantzun. Nahi adina galdetegi bidali ditzakezu, baina hilean gehienez 3.000 erantzun jaso ahal izango dituzu.
* Tarifak: 20€ hilabetean, 40€ hiruhilekoan, 130€ urtean
* Aparteko errekerimendua: softwarea dagokion domeinuan instalatzea (behin bakarrik). Prezioa: 150€

Aukera honek administrazio- eta kudeaketa-funtzionalitateak ditu:

* Erabiltzaileak administratu
* Erabiltzaileekin partekatu daitezkeen erabilera-baldintzak eta txantiloiak zehaztea, hala nola DPBLO.
* Taldean lan egin (erabiltzaileek beste erabiltzaileek sortutako galdetegiak kontsultatu/erabili ditzakete)
* SMTP konfiguratu eta aplikazioak sortzen dituen mezuak bidaltzeko helbidea definitu, adibidez "no-reply@zure_domeinua.com"
* Webgunea pertsonalizatu zure marka grafikoa jarriz
  * Hasiera-orriko testua
  * Webgunearen izena
  * Ikonoa (favicon-a)
  * Menuaren kolorea

