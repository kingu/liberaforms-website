---
title: 'Per què'
published: false
slug: perque
---

# Per què
## El context
Avui dia, gairebé tots els espais i projectes, independentment de si són col·lectius, associacions, cooperatives o centres educatius, tenen la necessitat de recollir dades per dur a terme les seves activitats: reserves, consultes, adhesions a manifestos, etc. Sovint, per desconeixement, es fa ús de programari privatiu d'empreses multinacionals que justament basen el seu negoci en la venda de les nostres dades a altres empreses perquè puguin dissenyar publicitats a mida, específiques per a cada persona. 

A través de la recollida de dades, empreses multinacionals com les GAFAM (Google, Apple, Facebook, Amazon, Microsoft) saben qui som, amb qui estem, les coses que ens preocupen, les nostres alegries, interessos i plans. Tot això, ha permès que siguin de les empreses més riques del món i que tinguin un poder de manipulació social sense precedents. Però cap dels productes i serveis d'aquestes grans empreses són gratis: els paguem amb les nostres dades, que són la matèria prima del capitalisme digital imperant.

=> [Les GAFAM per La Quadrature du net](https://liberaforms.org/ca/perque/gafam-quadrature-cat-a4.pdf)

D'altra banda, la llei de protecció de dades vigent obliga a recollir les dades de forma respectuosa. I a més, arrel de la Covid-19, fins i tot és obligatori fer reserva online per les activitats culturals.

=> [Covid-19 i reserves online](https://liberaforms.org/ca/covid)

## L'ètica
La concentració de poder en empreses tecnològiques s'ha aguditzat en els últims anys. Tot i així, encara hi ha persones que pensem que les tecnologies digitals són una oportunitat per reduir desigualtats, crear oportunitats i garantir aquest horitzó comú en una economia realment social i solidària, lluny de capitalismes depredadors. I és per això que creem i millorem béns digitals comuns, tecnologies ètiques en un context de cultura lliure. 

Construïm juntes aquest horitzó amb una ètica digital explícita: fem fora les GAFAM dels nostres espais i projectes, usem LiberaForms per confeccionar els nostres formularis!

=> [Prova LiberaForms!](https://usem.liberaforms.org/)  
=> [Consulta les condicions del servei](https://liberaforms.org/ca/serveis/)

O qualsevol altra eina de programari lliure com:
* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://framaforms.org>
* <https://www.limesurvey.org>
* <https://ohmyform.com>

## LiberaForms
LiberaForms és cultura lliure de proximitat i programari de quilòmetre zero sota llicència AGPLv3. Si bé el programa es va desenvolupar al 2019 desde la Lleialtec sota el nom de GNGforms, la idea arrencava dos anys abans. Quan al 2017 al barri de Sants s'estava lluitant per conservar l'Hortet de La Farga, es va crear un formulari de Google per recollir firmes. I va resultar que no només es pagava amb dades: a partir de cert nombre de formularis, o pagaves o l'empresa segrestava les dades. I així va ser com va néixer la idea de crear uns formularis ètics perquè els col·lectius del barri poguéssin recollir dades de forma respectuosa i sense sorpreses.

Més enllà de l'Hortet i de la Lleialtat Santsenca, LiberaForms és una eina que està a l'abast del veïnat i de les entitats i projectes que el vulguin fer servir. El programa està pensat per ser fàcil d'usar tant per les persones que ja usen formularis privatius com per altres persones que no tenen cap experiència prèvia. LiberaForms fa fàcil la creació, personalització i gestió dels formularis, és respectuós amb les dades de les persones i acompleix amb la llei de protecció de dades europea. Usem LiberaForms per a construir una societat més lliure, més ètica i més sobirana! :)
