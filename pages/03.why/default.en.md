---
title: Why
published: false
slug: why
---

# Why
## Context
In this day and age almost every entity whether they be associations, collectives, cooperatives, NGOs, educational centers ..., need to collect data to carry out their activities: reservations, opinion polls, manifest support, etc. Often, because of a lack of options or understanding, they use multinational proprietary software, software belonging to the same companies who base their business on the sale of our data to third parties, so to design personalized publicity specific to each person.

Through the hording of data these companies, such as the GAFAM (Google, Apple, Facebook, Amazon, Microsoft), know who we are, who we are with, the things that occupy us, our joys, interests and plans. This has made them the richest companies in the world and has given them unprecedented power to manipulate society. But neither their forms nor any of their other products are free: we pay with our data, the raw material of digital capitalism.

[GAFAM by La Quadrature du net (ca)](https://liberaforms.org/ca/perque/gafam-quadrature-cat-a4.pdf)

And on the other hand, let's not forget that data protection law also obliges us to collect data respectfully.

## Ethics
The concentration of power in these tech companies has only grown over the last few decades. Even so, there are still people who believe that digital technologies can be used to lower inequality, create opportunities, and build a sustainable society. This is our horizon and it is for these reasons that we develop digital commons and ethical technologies in the context of free culture.

Let's build this common horizon together by practicing explicit digital ethics: throw the GAFAM out of our spaces and projects, use LiberaForms!

* [Use LiberaForms!](https://usem.LiberaForms.org/)  
* [Read our service conditions](https://liberaforms.org/en/services)

Or any other libre form software like:
* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://framaforms.org>
* <https://www.limesurvey.org>
* <https://ohmyform.com>

## LiberaForms
LiberaForms is kilometre zero software licensed under the AGPLv3. Although  software development began in 2019 at La Lleialtat Santsenca (a Civic centre in Barcelona) and was first called GNGforms, the idea was spawned two years earlier. When in 2017 in the neighbourhood of Sants, Barcelona people where organizing to maintain the "Hortet de la Farga" (an urban vegetable garden), they used Google forms to collect signatures of support. And with time they discovered that not only were they paying with their data, but that after a certain number or signatures, the company demanded money, or else. And so the idea was born to build free, ethical form software so that the people of the neighbourhood could collect data respectfully and without surprises. 

Further a field than the Hortet and the Lleialtat Santsenca, LiberaForms is a tool available to anyone who chooses to use it. The software is designed to be easy to use for people who already use Google, and for those without previous experience. LiberaForms makes it easy to create, personalize, and manage forms. LiberaForms respects people's data and complies with European data protection law. Let's use free software to build a freer, more ethical, and more sovereign society! :)
