---
title: 'Por qué'
published: false
slug: porque
---

# Por qué
## El contexto
Hoy día, casi todos los espacios y proyectos independientemente de si son colectivos, asociaciones, cooperativas o centros educativos, tienen la necessidad de recoger datos para llevar a cabo sus actividades: reservas, consultas, adhesiones a manifiestos, etc. A menudo, por desconocimiento, se hace uso de software privativo de empresas multinacionales que justamente basan su negocio en la venta de nuestros datos a otras empresas para que puedan diseñar publicidades a medida, específicas para cada persona. 

A través de la recogida de datos, empresas multinacionales como las GAFAM (Google, Apple, Facebook, Amazon, Microsoft) saben quienes somos, con quién estamos, las cosas que nos preocupan, nuestras alegrías, intereses  y planes. Todo eso, ha permitido que sean de las empresas más ricas del planeta y que tengan un poder de manipulación social sin precedentes. Pero ninguno de los productos y servicios de estas grandes empresas son gratis: los pagamos con nuestros datos, que son la materia prima del capitalismo digital imperante.

=> [Las GAFAM por La Quadrature du net (ca)](https://liberaforms.org/ca/perque/gafam-quadrature-cat-a4.pdf) 

Por otro lado, la ley de protección de datos vigente obliga a recoger los datos de forma respetuosa. Y además, a raíz del Covid-19, hasta es obligatorio hacer reserva online para las actividades culturales.  

## La ética
La concentración de poder en empresas tecnológicas se ha agudizado en los últimos años. Aún así, todavía hay personas que pensamos que las tecnologías digitales son una oportunidad para reducir desigualdades, crear oportunidades y garantizar este horizonte común en una economía realmente social y solidaria, lejos de capitalismos depredadores. Y es por eso que creamos y mejoramos bienes comunes digitales, tecnologías éticas en un contexto de cultura libre. 

Construyamos juntas ese horizonte con una ética digital explícita: echemos a las GAFAM de nuestros espacios y proyectos, usemos LiberaForms para confeccionar nuestros formularios!

=> [Prueba LiberaForms!](https://usem.liberaforms.org/)  
=> [Consulta las condiciones de servicio](https://liberaforms.org/es/servicios)  

O cualquier otra herramienta de software libre como: 
* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://framaforms.org>
* <https://www.limesurvey.org>
* <https://ohmyform.com>

## LiberaForms
LiberaForms es cultura libre de proximidad y software de kilómetro cero bajo licencia AGPLv3. Si bien el programa se desarrolló en 2019 desde Lleialtec bajo el nombre de GNGforms, la idea se iniciaba dos años antes. Cuando en el 2017 en el barrio de Sants se estaba luchando para conservar l'Hortet de la Farga se creó un formulario con Google Forms para recoger firmas. Y resultó que no solo se pagaba con datos: a partir de cierto número de formularios, o pagabas o la empresa te secuestraba los datos. Y así fue como nació la idea de crear unos formularios éticos para que los colectivos del barrio pudieran recoger datos de forma respetuosa y sin sorpresas.  

Más allá del Hortet y de la Lleialtat Santsenca, LiberaForms es una herramienta que está al alcance del vecindario, de las entidades y proyectos que la quieran usar. El programa está pensado para ser fácil de usar tanto por las usuarias que ya usan formularios privativos como otras personas que no tengan ninguna experiencia previa. LiberaForms hace fácil la creación, personalitzación y gestión de los formularios, es respectuoso con los datos de las personas y cumple con la ley de protección de datos europea. ¡Usemos LiberaForms para construir una sociedad más libre, más ética y más soberana! :)
