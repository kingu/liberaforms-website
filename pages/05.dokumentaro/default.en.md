---
title: Docs
published: true
slug: documentation
---

# Documentation
Here we explain the documentation for the LiberaForms project.

## Users' guide
If you want to start creating forms with LiberaForms or administering an instance, take a look at the users' guide.  
[LiberaForms users' guide](https://docs.liberaforms.org)

## Code repository
If you want to create and maintain your own instance on a server, check the instructions on the code repository.  
[Code repository of LiberaForms](https://gitlab.com/liberaforms/liberaforms)

## Governance
At LiberaForms, we have agreed on a Social Contract and a Code of Conduct. If you want to get involved in the project, you will have to read and agree on the content of these two documents.

* [Social Contract](https://docs.liberaforms.org/en/socia-kontrakto.html)
* [Code of Conduct](https://docs.liberaforms.org/en/etiketo.html)

## Other documents
Other public documentation of LiberaForms, such as graphic documents, is available in the Arxius Oberts instance of the association LaLoka, the promoter of the LiberaForms project.  
[Arxius Oberts of LiberaForms](https://arxius.laloka.org/w/projectes/#LiberaForms)