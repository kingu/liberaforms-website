---
title: Participa
published: true
slug: participa
---

# Participa
Hay muchas maneras de participar en el proyecto LiberaForms.

## Usa y difunde
### Úsalo
Usar el programa es apoyarlo, y por lo tanto, mejorarlo. ¡Danos soporte creando y compartiendo formularios con LiberaForms!</br>
[Prueba LiberaForms!](https://usem.liberaforms.org/peticion)

### Promociona
¡Difundir el proyecto también es participar! Haz que otras personas, colectivos y entidades conozcan el proyecto. 

### Blogs
Si tienes un blog y nos quieres dedicar unas líneas, nos gustará leer tus impresiones. Infórmanos de que has escrito una entrada por el Fediverso o por correo electrónico. 

### Medios sociales
Si difundes por medios sociales, puedes a dar a conocer el proyecto con la etiqueta #LiberaForms. Y si nos quieres mencionar en el Fediverso, estamos en el nodo barcelona.social.</br>
[LiberaForms a la Fedi](https://barcelona.social/users/liberaforms)

## Reporta errores y propón mejoras
### En el código
Si eres una persona familiarizada con el desarrollo de software y encuentras algún error en el código o quieres proponer una mejora, te agradeceremos que nos lo hagas saber a través de la forja.</br>
[Forja LiberaForms](https://gitlab.com/liberaforms)

### En la documentación
Si encuentras algún error en la documentación o quieres proponer mejoras, te agradeceremos que nos lo hagas saber a través de la Fedi o por correo electrónico.</br>
[Documentación LiberaForms](https://docs.liberaforms.org/)

## Implícate
Implicarse en el proyecto significa contribuir a su desarrollo, tanto en la parte de la infraestructura como en la parte de los contenidos. Si quieres implicarte en LiberaForms, lee primero el Contrato Social y el Código de Conducta. 

* [Contrato Social](https://docs.liberaforms.org/es/socia-kontrakto.html) 
* [Código de Conducta](https://docs.liberaforms.org/es/etiketo.html)

## Fináncialo
### Contrata un servicio
Si después de probar el programa crees que harás un uso más intensivo o quieres personalizar LiberaForms bajo tu dominio, echa un ojo a los servicios que proponemos. Y si no te encajan con lo que necesitas, escrívenos un correo para poder personalizar el servicio a medida.</br>
[Servicios LiberaForms](https://liberaforms.org/es/servicios)

### Haz una donación
También puedes apoyar el proyecto haciendo una donación puntual o recurrente. LiberaForms es un proyecto impulsado por LaLoka, una associación sin ánimo de lucro que promueve la cultura libre. Las donaciones a LiberaForms se gestionan a través del OpenCollective de la associación LaLoka.</br>
[Haz una donación](https://opencollective.com/laloka)

