---
title: Projekto
published: true
slug: projekto
---

# La projekto
LiberaForms estas proksima liberkulturo kaj nula kilometro programilo sub licenco AGPLv3.

* [Aldonan valoron](aldonan-valoron)
* [Kronologio](kronologio)
* [Rigardu ĝin](media)
* [Provu LiberaForms!](https://usem.liberaforms.org/petu)  
* [Konsulti la kondiĉojn de servado](/servoj) 
    
## La origino
Se ĝi disvolvis en la 2019 jaro sub la nomo GNGforms, la ideo komencis du jarojn antaŭen. Kiam en la 2017 jaro al kvartalo de Sants (Barcelono, Katalanujo) estis batalanta per konservi lokan urban ĝardenon l'Hortet de la Farga, oni kreis Guglon formularon per kolekti subskribojn. Kaj rezulti ke ne nur oni paĝas kun datumoj: ekde certa nombro da respondojn aŭ oni paĝas aŭ la korporacio sekvestri la datumojn. Kaj tiel naskiĝis la ideon de krei etikajn formularojn do kolektivoj al nia kvartalo povis kolekti datumojn respekteme kaj sensurprize.

## La kunteksto
Nuntempe, preskaŭ ĉiuj kunvenejoj kaj projektoj, sendepende se ĝi estas kolektivoj, asocioj, kooperativoj aŭ edukado centroj, ĝi havas la bezono de kolekti datumojn per fari vian aktivadojn: rezervoj, konsultoj, aliĝoj al manifestoj, ktp. Ofte, per malkonigo, oni uzas malliberan programaron de multnaciaj korporacioj kiu precize bazi sian entreprenon en la vendo de niaj datumoj al aliaj korporacioj per povi desegni laŭmende reklamojn, specife per ĉiu persono. 

Tra datumojn kolektadon, multnaciaj korporacioj kiel GAFAMiloj (Google, Apple, Facebook, Amazon, Microsoft) konas kiu vi estas, kun kiu vi estas, konas aferojn kiu maltrankviligas nin, nian feliĉojn kaj nian projecktojn. Ĉio ĉi, eblegas ke tiuj korporacioj estas de plej riĉaj en la mondo kaj ke ĝi havas senprecedence socian manipulan potencon. Sed neniu de ĉi tiuj produktoj kaj servoj de ĉi tiuj grandaj korporacioj estas senkosta: ni pagas ĝin kun nian datumojn, kiu estas la krudmaterialo de la ĉifereca reganta kapitalismo.

[GAFAMiloj per Komun](https://wiki.komun.org/books/seguridad-y-privacidad/page/kiel-liberigi-nin-de-gafam) 

Aliflanke, la aktuala eŭropa leĝo de protektado de datumoj devigas a la respektema kolektado de datumoj. Krome, pro la kronviruso, êc estas deviga fari rezervojn interrete per partopreni de kulturaj aktivadoj. 

## La etiko
La koncentrado de potenco en teknologiaj korporacioj akrigiĝis dum lastaj tempoj. Malgraŭ tio, ankoraŭ estas personoj kiu pensas ke la ĉiferecaj teknologioj eblas okazo per redukti malegalajn, krei ŝancojn kaj garantii ĉi tiu komunan horizonton en reale socia kaj solidareca ekonomio, malproksime de predantaj kapitalismoj. Pro tio, ni kreas kaj plibonigas komunajn ĉifericajn varojn, etikajn teknologiojn en liberkultura konteksto.

Konstruu kune ĉi tiu horizonton kun eksplicita ĉifereca etiko: forpelu la GAFAM de niaj kunvenejoj kaj projektoj, uzu LiberaForms por krei viajn formularojn!

## Aliaj iloj
Ni ne estas la sola projekto kiu oferas formularojn. Se LiberaForms ne adaptas al viaj bezonoj, eble interesas al vi konsideri aliajn liberajn ilojn kiu estas kreintaj pro aliaj personoj: 

* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://yakforms.org/>
* <https://www.limesurvey.org>
* <https://ohmyform.com>
