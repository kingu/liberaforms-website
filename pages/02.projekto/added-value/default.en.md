---
title: 'Added value'
published: true
slug: added-value
---

# Added value

* LiberaForms is free culture. The code is licensed under the AGPLv3 and is available at this public repository, and it can be installed and audited by anyone with the know how. Other material created such as howtos and promotional media are licensed under CC BY-SA. 

* At LiberaForms we believe that every person has the right to the same features independently of their purchasing power. For that reason, everybody who uses LiberaForms has access to the same features. Those people who decide to install their own LiberaForms can use the more advanced features of administration and management.

* LiberaForms is designed to respect your digital projects and environments. Forms do not include branding of any type and are void of publicity aggression directed at end users. At LiberaForms we put into practice ethical design.
  * [Ethical design](https://2017.ind.ie/ethical-design/)

* LiberaForms uses sovereign and decentralized tools to build it's online presence. This webiste is built with Grav and does not contain trackers or publicity. If  you would like to get in contact with us, we use email. Our social media account is hosted at barcelona.social, part of the federated network of free social media called the FEDIVERSE. LiberaForms complies with and extends the recommendations of the RAP by not using corporate asocial media platforms.
  * [Fediverse (catalan)](https://fedi.cat)
  * [The RAP (Résistance à l’Agression Publicitaire)](https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/)


* LiberaForms aims to be sustainable through the services we offer to people and entities who wish to make use of it, whether personally and/or intensively. LibreForms does not receive money from venture capitalists and is not a 'start-up' type company.
