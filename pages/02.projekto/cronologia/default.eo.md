---
title: Kronologio
slug: kronologio
---

# Kronologio
Ĉi tie vi trovos resumon de la laŭtempa evoluo de la projekto LiberaForms.

## 2017
Ideo kaj kreacio de la programaro LiberaForms sub la nomo GNGForms (rekursia humuro: GNGforms is Not Googleforms).

## 2019
Ni instalas la unuajn aperojn de LiberaForms dum la projekto Equipaments Lliures. Grupoj de la kvartaloj Sants kaj El Guinardó (Barcelono) komencas uzi ilin. Plue, la nederlanda ekologiista ento Code Rood agordas ĝian aperon.

* [Projekto Equipaments Lliures](https://equipamentslliures.cat/)
* [Projekto Code Rood](https://code-rood.org)

## 2020
### Aliaj aperoj
Aliaj fediversuloj agordas iliajn aperojn:

* [Apero de Komun](https://forms.komun.org/)
* [Afiŝo ĉe _ voidNull [Hispane]](https://voidnull.es/instalar-liberaforms-en-debian-10-para-olvidarte-de-google-forms/)

### Financado kaj plibonigoj
Ni prezentas proponon al NLnet sub la ['Privacy & Trust Enhancing Technologies'](https://nlnet.nl/PET/) projekto por ke ili financos la evoluigo de LiberaForms kaj ili akceptas ĝin. Ekde Novembro de 2020 ĝis Oktobro de 2021 ni havas la monan subtenon de ĉi tiu fondaĵo. Listo de plibonigoj al la projekto danke al la subteno de NLnet:

* Monitorado
* Migrado de la datumbazo
* Enpakaĵo
* Fora Objekta Konservado
* Mesaĝado
* Tabeloj / Montrado de datumoj
* Testoj
* Sekureca revizio
* Diskonigo per RSS
* Formularo-modeloj
* Alirebleca revizio
* Regado
* Dokumentado
* Komunikado
* Partoprenado

![NLnet logo](https://nlnet.nl/image/logo_nlnet.svg)

### De GNGForms al LiberaForms
Teamano de NLnet komentas: "Ĉu vi pensis ke eble ne estas bona ideo inkludi la nomon de la internacia firmao en la nomon de via projekto? Se vi volas renomi ĝin, nun estus bona momento". Dirite kaj farite. En malpli ol mezhoro ni trovis la nomon LiberaForms kaj ni aĉetis la domajnon.

## 2021
Ekde Januaro ĝis Oktobro ni pluas evoluigante la projekton kun la subteno de la Fondaĵo NLnet. Dum la printempo, alia fediversulo agordas rian instancon kaj skribas pri la instalado ĉe esferas.org. Cetere, la asocio Sidastudi iĝas nia unua kliento kaj, kun la eniĝo de [Porru](mailto:porru@liberaforms.org) en la teamon, ni agordas novan instancon de LiberaForms por eŭskaj uzantoj.

* [Afiŝo ĉe esferas.org [Hispane]](https://esferas.org/msqlu/2021/05/01/liberaforms-un-servicio-autoalojado-de-formularios-web/)
* [Asocio Sidastudi](http://www.sidastudi.org/)
* [Eŭska instanco de LiberaForms](https://erabili.liberaforms.org/)