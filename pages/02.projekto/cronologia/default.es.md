---
title: Cronología
slug: cronologia
---

# Cronología
En este apartado encontraréis un resumen de la evolución cronológica del proyecto LiberaForms.

## 2017
Idea y creación del software LiberaForms bajo el nombre de GNGforms (humor recursivo: GNGforms is Not Googleforms). 

## 2019
Instalamos las primeras instancias de formularios durante el proyecto Equipaments Lliures. Entidades de los barrios de Sants y de El Guinardó empiezan a hacer uso del programa. Además, la entidad ecologista holandesa Code Rood monta su propia instancia. 

* [Proyecto Equipaments Lliures](https://equipamentslliures.cat/)
* [Proyecto Code Rood](https://code-rood.org)

## 2020
### Otras instancias
Otras vecinas fediversales se montan sus instancias: 

* [Instancia de Komun](https://forms.komun.org/)
* [Entrada en _ voidNull](https://voidnull.es/instalar-liberaforms-en-debian-10-para-olvidarte-de-google-forms/)

### Financiación y mejoras
Presentamos el proyecto a [NLnet](https://nlnet.nl/) y nos lo acceptan. De noviembre 2020 a octubre 2021, contamos con el suporte económico de esta fundación. Lista de mejoras del proyecto gracias al soporte de NLnet:

* Monitoritzación
* Migración de la base de datos
* Empaquetado
* Almacenaje remoto
* Mensajería
* Tablas / visualitzación de datos
* Tests
* Auditoria de seguridad
* Compartir por RSS
* Plantillas
* Auditoria de accessibilidad
* Governanza
* Documentación
* Comunicación
* Participación

![NLnet logo](https://nlnet.nl/image/logo_nlnet.svg)

### De GNGforms a LiberaForms
Un miembro del equipo de NLnet nos hizo una observación: "¿No os incomoda llevar el nombre de una multinacional incorporado? Si le queréis cambiar el nombre, ahora sería un buen momento". Dicho y hecho. En menos de media hora se nos ocurrió LiberaForms y compramos el dominio. 

## 2021
De enero a octubre, continuamos el desarrollo del proyecto con el soporte de la Fundación NLnet. Durante la primavera, otra fedizen montó su propia instancia y escribió sobre la instalación en esferas.org. Además, la asociación Sidastudi se convirtió en nuestra primera clienta y, con la incorporación de [Porru](mailto:porru@liberaforms.org) al equipo, hemos habilitado una nueva instancia en euskera. 

* [Entrada a esferas.org](https://esferas.org/msqlu/2021/05/01/liberaforms-un-servicio-autoalojado-de-formularios-web/)
* [Associación Sidastudi](http://www.sidastudi.org/)
* [Instancia de LiberaForms en euskera](https://erabili.liberaforms.org/)