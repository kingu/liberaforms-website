---
title: Cronologia
slug: cronologia
---

# Cronologia
En aquest apartat hi trobareu un resum de l'evolució cronològica del projecte LiberaForms.

## 2017
Idea i creació del programari LiberaForms sota el nom de GNGforms (humor recursiu: GNGforms is Not Googleforms). 

## 2019
Instal·lem les primeres instàncies de formularis durant el projecte Equipaments Lliures. Entitats dels barris de Sants i de El Guinardó en comencen a fer ús. A més, l'entitat ecologista holandesa Code Rood munta la seva pròpia instància. 

* [Projecte Equipaments Lliures](https://equipamentslliures.cat/)
* [Projecte Code Rood](https://code-rood.org)

## 2020
### Altres instàncies
Altres veïnes fediversals munten les seves instàncies: 

* [Instància de Komun](https://forms.komun.org/)
* [Entrada a _ voidNull](https://voidnull.es/instalar-liberaforms-en-debian-10-para-olvidarte-de-google-forms/)

### Finançament i millores
Presentem el projecte a [NLnet](https://nlnet.nl/) i ens l'accepten. De novembre del 2020 a octubre de 2021, comptem amb el suport econòmic d'aquesta fundació. Llista de millores de projecte gràcies al suport de NLnet:

* Monitorització 
* Migració de la base de dades
* Empaquetat 
* Emmagatzematge remot
* Missatgeria
* Taules / visualització de dades
* Tests
* Auditoria de seguretat
* Compartir per RSS
* Plantilles
* Auditoria d'accessibilitat
* Governança
* Documentació
* Comunicació
* Participació

![NLnet logo](https://nlnet.nl/image/logo_nlnet.svg)

### De GNGforms a LiberaForms
Un membre de l'equip NLnet ens fa una observació: "No us incomoda portar el nom d'una multinacional incorporat? Si li voleu canviar el nom, ara seria un bon moment". Dit i fet. En menys de mitja hora se'ns va ocorrer LiberaForms i vam comprar el domini. 

## 2021
De gener a octubre, continuem el desenvolupament del projecte amb el suport de la Fundació NLnet. Durant la primavera, una altra fedizen munta la seva pròpia instància i escriu sobre la instal·lació a esferas.org. A més, l'associació Sidastudi es converteix en la nostra primera clienta i, amb la incorporació d'en [Porru](mailto:porru@liberaforms.org) a l'equip, habilitem una nova instància de LiberaForms en eusquera. 

* [Entrada a esferas.org](https://esferas.org/msqlu/2021/05/01/liberaforms-un-servicio-autoalojado-de-formularios-web/)
* [Associació Sidastudi](http://www.sidastudi.org/)
* [Instància de LiberaForms en eusquera](https://erabili.liberaforms.org/)
