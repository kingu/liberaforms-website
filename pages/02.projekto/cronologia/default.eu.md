---
title: Kronologia
slug: kronologia
---

# Kronologia
Hemen LiberaForms proiektuaren eboluzio kronologiko bat aurkituko duzu.

## 2017
LiberaForms softwarearen ideia eta sorkuntza, GNGForms izenpean (umore errekurtsiboa: GNGforms is Not Googleforms).

## 2019
Equipaments Lliures proiektuan zehar lehenengo LiberaForms instantziak instalatzen ditugu. Bartzelonako Sants eta El Guinardó auzoetako taldeak instantziak erabiltzen hasten dira. Gainera, Code Rood erakunde ekologista herbeheretarrak bere instantzia propioa muntatzen du.

* [Equipaments Lliures proiektua](https://equipamentslliures.cat/)
* [Code Rood proiektua](https://code-rood.org)

## 2020
### Beste instantziak
Beste fedibertsolari batzuk haien instantziak muntatzen dituzte:

* [Komun-en instantzia](https://forms.komun.org/)
* [_ voidNull webguneko bidalketa [gazteleraz]](https://voidnull.es/instalar-liberaforms-en-debian-10-para-olvidarte-de-google-forms/)

### Finantziazioa eta hobekuntzak
Proposamen bat aurkeztu diogu NLnet-i ['Privacy & Trust Enhanensa Technologies'](https://nlnet.nl/PET/) proiektuaren ildoan LiberaForms-en garapena finantzatzeko eta onartu egin dute. 2020ko azarotik 2021eko urrira arte, fundazio honen laguntza ekonomikoa dugu. NLnet-i esker proiektuan gehitu(ko) ditugun hobekuntzen zerrenda:

* Monitorizazioa
* Datu-basearen migrazioa
* Paketatzea
* Urruneko Objektu Biltegiratzea
* Mezularitza
* Taulak / Datuen bistaratzea
* Probak
* Segurtasun ikuskaritza
* RSSrekin partekatzea
* Galdetegi-txantiloiak
* Erabilerraztasun ikuskaritza
* Gobernantza
* Dokumentazioa
* Komunikazioa
* Parte-hartzea

![NLnet logo](https://nlnet.nl/image/logo_nlnet.svg)

### GNGForms-etik LiberaForms-era
NLnet-eko kide batek iruzkin bat egin zigun: "Pentsatu duzue akaso ez dela ideia ona multinazionalaren izena zuen proiektuaren izenean sartzea? Izena aldatu nahi baduzue, orain une egokia izango litzateke." Esan eta egin. Ordu-erdi baino gutxiagoan, LiberaForms izena bururatu zitzaigun eta domeinua erosi genuen.

## 2021
Urtarriletik urrira bitartean NLnet Fundazioaren laguntzarekin proiektua garatzen jarraitzen dugu. Udaberrian, beste fedibertsolari batek bere instantzia propioa muntatu eta instalazioari buruz esferas.org webgunean idazten du. Gainera, Sidastudi elkartea gure lehen bezero bihurtzen da, eta [Porru](mailto:porru@liberaforms.org) gure taldean sartu zenean, LiberaForms-en instantzia berri bat sortzen dugu euskaraz.

* [esferas.org webguneko bidalketa [gazteleraz]](https://esferas.org/msqlu/2021/05/01/liberaforms-un-servicio-autoalojado-de-formularios-web/)
* [Sidastudi elkartea](http://www.sidastudi.org/)
* [LiberaForms-en euskarazko instantzia](https://erabili.liberaforms.org/)