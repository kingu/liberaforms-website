---
title: Covid-19
---

# Covid-19

La crisi sanitària també ha generat noves necessitats als equipaments i espais de pública concurrència a les quals LiberaForms pot donar resposta. Tal i com s’estableix al documents:

[CRITERIS CC OBERTURA RETORN valid 6-5-20Def.pdf](CRITERIS%20CC%20OBERTURA%20RETORN%20valid%206-5-20Def.pdf)

Pàgina 14.

> Les activitats obertes al públic s’han de reservar online amb antelació, així com la reserva i pagament de cursos i tallers tambe s’ha de fer telemàticament. La necessitat de reserva prèvia cal que es comuniqui amb insistència, ja que és la millor manera de mantenir limitats els aforaments a l’interior dels espais on es desenvolupa l’acti itat i que no hi hagi aglomeracions a l’entrada. La reserva prèvia ha d’incorporar la recollida de dades de contacte (telèfon i correu electrònic) per fer possible la comunicació en cas d’identificació de casos positius.

[Criteris per realitzar activitat cultural.pdf](Criteris%20per%20realitzar%20activitat%20cultural.pdf)

Pàgina 4.
> La  necessitat de  reserva  prèvia  cal  que  es  comuniqui  amb  insistència,  ja  que  és  la  millor manera  de  mantenir limitats els  aforaments  al’interior  dels espais  on  es desenvolupa l’activitat i que no hi hagi aglomeracions a l’entrada. La  reserva prèvia ha d’incorporar la recollida de dades de contacte (telèfon i correu electrònic) per fer possible la comunicació en cas d’identificació de casos positius, sempre dins el marc de la Llei Orgànica de Protecció de Dades.

Pàgina 5.
> L’accés als equipaments es farà amb reserva prèvia per franges horàries com a criteri  general.  La  necessitat  de  reserva  prèvia  es  farà  constar  en  els  elements de comunicació. L’admissió de públic sense reserva serà possible només en cas que l’ocupació ho admeti. En tot cas caldrà recollir dades personals, bé al fer la reserva bé a l’accedir a l’equipament, per a la comunicació en cas d’identificar casos positius.

Com llegim en aquests documents, l'Administració obliga a entitats i projectes a fer reserves online. Però si no proposa cap solució és molt probable que la majoria faci servir Google Forms per a complir amb els requisits de l’Ajuntament. És per això que us proposem usar LiberaForms, una solució sobirana i respectuosa amb les dades de les persones que participen dels nostres espais i projectes.
